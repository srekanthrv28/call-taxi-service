/* @flow */
'use strict';

import logger from 'winston';
import location_handler from '../location_handler_init/location_handler';
import * as request_validator from '../request/location_handler_request_validator';
import * as location_handler_entity_wrapper from './location_handler_entity_wrapper';
import {LocationDistanceHandlerRequest} from "../request/location_distance_handler_request";
import {LocationHandlerResponse} from "../response/location_handler_response";
import {LocationDistanceHandlerEntity} from '../entity/location_distance_handler_entity';
import R from 'ramda';
const distance_in_meters = 200000;

export function caluclate_distance(request: LocationDistanceHandlerRequest): Promise<LocationHandlerResponse | Error> {
  const _x = logger.info(`Got the request to caluclate the distance: ${R.toString(request)}`);
  return request_validator.validate_distance_location_handler_request_async(request)
    .then(location_handler_entity_wrapper.from_location_handler_web_request_to_distance_entity)
    .then(get_distance)
    .then(R.curry(prepare_and_response)(request));
}

function get_distance(location_distance_handler_entity: LocationDistanceHandlerEntity): Promise<> {
  return location_handler.distanceMatrix({
      origins: [
        location_distance_handler_entity.origin
      ],
      destinations: location_distance_handler_entity.destination

    }).asPromise();
}

function prepare_and_response(request: LocationDistanceHandlerRequest, location_handler_response: *): Promise<>{
  const row =  R.head(location_handler_response.json.rows);
  const all_cabs_data = R.map(R.curry(get_value), row.elements);
  const distance_result = get_near_distance_cab(row);
  if(R.equals(distance_result.status, "ZERO_RESULTS")) {
    return Promise.reject({reason: `Unable to caluclate the distance`, type: "INVALID_LAT_LNG"});
  }
  return check_if_the_distance_more_than_specified(distance_result.distance.value, all_cabs_data, request)
}

function get_near_distance_cab(row: *): Promise<> {
  const sorted_by_distance = R.sortBy(R.curry(get_value), row.elements);
  const distance_result = R.head(sorted_by_distance);
  return distance_result;
}

function get_value(value: *): number {
   return (R.prop("value", R.prop("distance", value)));
}

function check_if_the_distance_more_than_specified(distance: number, all_cabs_data: *, request: LocationDistanceHandlerRequest): Promise<>{
  const cab_index = R.indexOf(distance, all_cabs_data);
  if(distance > distance_in_meters) {
    return Promise.reject({reason: `Long Distance`, type: "INVALID_DISTANCE"});
 }
 const cab_details = request.destination[cab_index];
 const _x = logger.info(`Sending nearest cab details: ${R.toString(cab_details)}`);
 return Promise.resolve({is_valid_distance: true, distance: distance, cab_details: cab_details});
}
