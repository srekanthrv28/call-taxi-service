/* @flow */
'use strict';

import location_handler from '../location_handler_init/location_handler';
import * as request_validator from '../request/location_handler_request_validator';
import * as location_handler_entity_wrapper from './location_handler_entity_wrapper';
import {LocationHandlerRequest} from "../request/location_handler_request";
import {LocationHandlerResponse} from "../response/location_handler_response";
import {LocationHandlerEntity} from '../entity/location_handler_entity';
import logger from 'winston';
import R from 'ramda';

export function find_user_location(request: LocationHandlerRequest): Promise<LocationHandlerResponse | Error> {
  const _x = logger.info(`Got the request to the user geo location: ${R.toString(request)}`);
  return request_validator.validate_send_location_handler_request_async(request)
    .then(location_handler_entity_wrapper.from_location_handler_web_request_to_entity)
    .then(find_location)
    .then(prepare_geo_location_response)
    .then(location_send_success);
}

function location_send_success(location_handler_response: LocationHandlerResponse): LocationHandlerResponse {
  const _x = logger.info(`Successfully found location: ${R.toString(location_handler_response)}`);
  return location_handler_response;
}

function find_location(location_handler_entity: LocationHandlerEntity): Promise<> {
  return location_handler.geocode({address: location_handler_entity.address}).asPromise();
}

function prepare_geo_location_response(location_handler_response: *): Promise<>{
  if(R.isEmpty(location_handler_response.json.results)) {
     return Promise.reject({reason: `Invalid Location`, type: "INVALID_USER_LOCATION"});
   }
  const location_result = R.head(location_handler_response.json.results);
  return location_result.geometry.location;
}
