/* @flow */
'use strict';

import {LocationHandlerRequest} from "../request/location_handler_request";
import {InitialLocationHandlerResponse, LocationHandlerResponse} from "../response/location_handler_response";
import {InitialLocationHandlerEntity, LocationHandlerEntity} from '../entity/location_handler_entity';
import {InitialLocationDistanceHandlerEntity, LocationDistanceHandlerEntity} from '../entity/location_distance_handler_entity';
import R from 'ramda';

export function from_location_handler_entity_to_response(location_handler_entity: LocationHandlerEntity): LocationHandlerResponse {
  return R.pick(R.keys(InitialLocationHandlerResponse), location_handler_entity);
}

export function from_location_handler_web_request_to_entity(request: LocationHandlerRequest): LocationHandlerEntity {
  return R.pick(R.keys(InitialLocationHandlerEntity), request);
}


export function from_location_handler_web_request_to_distance_entity(request: LocationHandlerRequest): LocationDistanceHandlerEntity {
  return R.pick(R.keys(InitialLocationDistanceHandlerEntity), request);
}
