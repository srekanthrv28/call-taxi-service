/* @flow */
'use strict';

import logger from 'winston';
import google_maps from '@google/maps';
const google_maps_key = 'AIzaSyCnTqyVeLb88etfWW5UsKV1MSjl5L2U_qM';

const _log_before_location_handler_in_it = logger.info("Initializing the location service...");

const location_handler = google_maps.createClient({key: google_maps_key, Promise: Promise});

const _log_after_location_handler_in_it = logger.info("Location service Initialized!!");

export default location_handler;
