/* @flow */
'use strict';

import {
  LocationHandlerRequest,
  LocationHandlerRequestConstraints,
} from '../request/location_handler_request';

import {
  LocationDistanceHandlerRequest,
  LocationDistanceHandlerRequestConstraints,
} from '../request/location_distance_handler_request';

import validate from 'validate.js';
import R from 'ramda';

export function validate_send_location_handler_request_async(location_handler_request: LocationHandlerRequest): Promise<> {
  const validation_result: mixed = validate.validate(location_handler_request, LocationHandlerRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(location_handler_request);
  }
  return send_error(validation_result);
}

export function validate_distance_location_handler_request_async(location_distance_handler_request: LocationDistanceHandlerRequest): Promise<> {
  const validation_result: mixed = validate.validate(location_distance_handler_request, LocationDistanceHandlerRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(location_distance_handler_request);
  }
  return send_error(validation_result);
}

function send_error(validation_result: mixed): Promise<>  {
  return Promise.reject({reason: "Invalid Request", type: "INVALID_REQUEST", fields: validation_result});
}
