/* @flow */
'use strict';
import {LocationLatLngRequest} from './location_lat_lng_request';

export interface LocationDistanceHandlerRequest {
  +origin: LocationLatLngRequest,
  +destination: Array<LocationLatLngRequest>
}

export const InitialLocationDistanceHandlerRequest: LocationDistanceHandlerRequest = {
  origin: {lat: '', lng: ''},
  destination: [{lat: '', lng: ''}]
};

export const LocationDistanceHandlerRequestConstraints  = {
  origin: {
    presence: true
  },
  destination: {
    presence: true
  }
}
