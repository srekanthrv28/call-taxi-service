/* @flow */
'use strict';

export interface LocationHandlerRequest {
  +address: string
}

export const InitialLocationHandlerRequest: LocationHandlerRequest = {
  address: ''
};

export const LocationHandlerRequestConstraints  = {
  address: {
    presence: true
  }
}
