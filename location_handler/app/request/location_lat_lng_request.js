/* @flow */
'use strict';

export interface LocationLatLngRequest {
  +lat: string,
  +lng: string
}
