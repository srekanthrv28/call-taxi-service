/* @flow */
'use strict';

import * as find_location_interactor from '../interactor/find_location_interactor';
import * as location_distance_interactor from '../interactor/location_distance_interactor';
import {LocationHandlerRequest} from '../request/location_handler_request';
import {LocationDistanceHandlerRequest} from '../request/location_distance_handler_request';

export function find_user_location(request_model: LocationHandlerRequest): Promise<> {
  return find_location_interactor.find_user_location(request_model);
}

export function find_distance(request_model: LocationDistanceHandlerRequest): Promise<> {
  return location_distance_interactor.caluclate_distance(request_model);
}
