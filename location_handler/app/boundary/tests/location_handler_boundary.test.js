/* @flow */
'use strict';

import {it, describe} from 'mocha';
import * as boundary from '../location_handler_boundary';
import {LocationHandlerRequest} from '../../request/location_handler_request';

import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);

const get_location_handler_request = (): LocationHandlerRequest => {
  return {
    address: '1600 Amphitheatre Parkway, Mountain View, CA'
  }
};

async function find_user_location(create_location_handler_request: LocationHandlerRequest): Promise<> {
  return await boundary.find_user_location(create_location_handler_request);
}

const _test_location_handler = describe("Send LocationHandler - Boundary", (): void => {
  const test_location_handler = it('Should create location_handler', (): Promise<> => {
    const test_request: LocationHandlerRequest = get_location_handler_request();
    return find_user_location(test_request);
  });
  return test_location_handler;
});
