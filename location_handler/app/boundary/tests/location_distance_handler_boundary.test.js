/* @flow */
'use strict';

import {it, describe} from 'mocha';
import * as boundary from '../location_handler_boundary';
import {LocationDistanceHandlerRequest} from '../../request/location_distance_handler_request';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);

const get_location_handler_request = (): LocationDistanceHandlerRequest => {
  return {
    origin: {lat: '12.9260', lng: '77.6762'},
    destination: [{lat: '13.0358', lng: '77.5970', cab_id: 1}, {lat: '12.9279', lng: '77.6271', cab_id: 2}]
  }
};

async function find_user_location(create_location_handler_request: LocationDistanceHandlerRequest): Promise<> {
  return await boundary.find_distance(create_location_handler_request);
}

const _test_location_handler = describe("Distance LocationHandler - Boundary", (): void => {
  const test_location_handler = it('Distance location handler', (): Promise<> => {
    const test_request: LocationDistanceHandlerRequest = get_location_handler_request();
    return find_user_location(test_request);
  });
  return test_location_handler;
});
