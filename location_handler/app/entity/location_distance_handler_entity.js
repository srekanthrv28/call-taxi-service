/* @flow */
'use strict';

export interface LocationDistanceHandlerEntity {
  +origin: string,
  +destination: string
}

export const InitialLocationDistanceHandlerEntity: LocationDistanceHandlerEntity = {
  origin: '',
  destination: ''
}
