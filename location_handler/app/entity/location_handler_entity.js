/* @flow */
'use strict';

export interface LocationHandlerEntity {
  +address: string
}

export const InitialLocationHandlerEntity: LocationHandlerEntity = {
  address: ''
}
