/* @flow */
'use strict';

export interface LocationHandlerResponse {
  +lat: string,
  +lng: string
}

export const InitialLocationHandlerResponse: LocationHandlerResponse = {
  lat: '',
  lng: ''
};
