/* @flow */
'use strict';

export interface UserResponse{
  +email: string,
  +phone_number: string
}
