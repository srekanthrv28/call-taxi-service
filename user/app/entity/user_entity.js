/* @flow */
'use strict';


export interface UserEntity {
  +email: string,
  +phone_number: string
}
