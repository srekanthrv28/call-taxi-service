/* @flow */
'use strict';

export interface FindUserRequest {
  +email: string
}

export const InitialFindUserRequest: FindUserRequest = {
  email: ''
}

export const FindUserRequestConstraints  = {
  email: {
    presence: true,
    length: {
      min: 5,
      max: 512
    }
  }
}
