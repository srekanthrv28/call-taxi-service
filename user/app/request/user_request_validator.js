/* @flow */
'use strict';

import {FindUserRequest, FindUserRequestConstraints} from '../request/find_user_request';
import * as validate from 'validate.js';


export function validate_find_user_request_async(find_user_request: FindUserRequest): Promise<> {
  return validate.async(find_user_request, FindUserRequestConstraints, {format: "grouped"});
}
