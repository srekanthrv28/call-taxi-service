/* @flow */
'use strict';

import { CreateUserRequestModel,CreateUserRequestModelConstraints} from '../request/create_user_request_model';
import * as validate from 'validate.js';

export function validate_request_async(create_user_request: CreateUserRequestModel): Promise {
  return validate.async(create_user_request, CreateUserRequestModelConstraints, {format: "flat"});
}
