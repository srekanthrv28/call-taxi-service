/* @flow */
'use strict';

export interface CreateUserRequestModel {
  +email: string,
  +phone_number?: string
}

export const CreateUserRequestModelConstraints  = {
  email: {
    presence: true,
    email: true
  },
  phone_number: {
    length: {
      minimum: 5,
      maximum: 20
    }
  }
}
