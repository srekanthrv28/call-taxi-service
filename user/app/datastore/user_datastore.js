/* @flow */
'use strict';

import {UserEntity} from '../entity/user_entity';
import * as in_memory_datastore from './in_memory_user_datastore';
import logger from 'winston';
import R from 'ramda';

const datastore = in_memory_datastore;

export function create_datastore_user(user_entity: UserEntity): Promise {
  const _x = logger.info(`CREATING USER WITH EMAIL: ", ${R.toString(user_entity)}`);
  return datastore.create_datastore_user(user_entity);
}

export function find_datastore_user_by_email(user_entity: UserEntity): Promise {
  const _x = logger.info(`Getting USER WITH ID: ", ${user_entity.email}`);
  return datastore.get_user_data_by_email(user_entity);
}
