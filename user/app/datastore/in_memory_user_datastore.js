/* @flow */
'use strict';

import * as logger from 'winston';
import {UserEntity} from '../entity/user_entity';
import * as datastore from 'memory-cache';
import uuid_v4 from 'uuid/v4';
import R from 'ramda';

export function create_datastore_user(user_entity: UserEntity): Promise {
  if (does_user_already_exist(user_entity)) {
    const _a = logger.error(`User with email: ${user_entity.email} already exists`);
    return Promise.reject(`User with email: ${user_entity.email} already exists`);
  }
  return Promise.resolve(create_new_datastore_user(user_entity));
}

function does_user_already_exist(user_entity: UserEntity): boolean {
  if (datastore.get(user_entity.email)) {
    return true;
  }
  return false;
}

function create_new_datastore_user(user_entity: UserEntity): UserEntity {
  const new_user_entity: UserEntity = R.merge(user_entity, {user_id: uuid_v4()});
  const _user =  datastore.put(user_entity.email, new_user_entity)
  return datastore.get(user_entity.email);
}

function does_user_by_email_already_exist(user_entity: UserEntity): boolean {
  return R.ifElse(R.curry(datastore.get), R.T, R.F)(user_entity.email);
}

export function get_user_data_by_email(user_entity: UserEntity): Promise<UserEntity | Error> {
  return datastore.get(user_entity.email);
}

export function find_datastore_user_by_email(user_entity: UserEntity): Promise<UserEntity | Error> {
  return R.ifElse(R.curry(does_user_by_email_already_exist), get_user_data_by_email, error_user_does_not_exist)(user_entity);
}

function error_user_does_not_exist(user_entity: UserEntity): Promise<Error> {
  const _a = logger.error(`User with email ${R.toString(user_entity.email)} does not exists.`);
  return Promise.reject({reason: `User with email: ", ${user_entity.email} does not exists`, type: "INVALID_REQUEST"});
}
