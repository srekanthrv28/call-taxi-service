/* @flow */
'use strict';

import {FindUserRequest} from '../request/find_user_request';
import {InitialUserResponse, type UserResponse} from '../response/user_response';
import {InitialUserEntity, type UserEntity} from '../entity/user_entity';
import R from 'ramda';
import logger from 'winston';

export function from_user_entity_to_response(user_entity: UserEntity): UserResponse {
  return R.pick(R.keys(InitialUserResponse), user_entity);
}

export function from_user_request_to_entity(request: CreateUserRequest): UserEntity {
  const _x = logger.info(`test user with request: ${JSON.stringify(request)}`);
  return R.pick(R.keys(InitialUserEntity), request);
}

export function from_find_user_request_to_entity(request: FindUserRequest): UserEntity {
  return R.merge(InitialUserEntity, {id: request.user_id});
}
