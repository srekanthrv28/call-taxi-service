/* @flow */
'use strict';

import * as logger from 'winston';

import {FindUserRequest} from '../request/find_user_request';
import {UserResponse} from '../response/user_response';
import * as request_validator from '../request/user_request_validator';
import * as user_datastore from '../datastore/user_datastore';
import R from 'ramda';

export function find_user(request: FindUserRequest): Promise<> {
  const _x = logger.info(`Finding user with request: ${R.toString(request)}`);
  return request_validator.validate_find_user_request_async(request)
    .then(user_datastore.find_datastore_user_by_email(request))
    .then(user_fetch_success);
}

function user_fetch_success(user_response: UserResponse): UserResponse {
  const _x = logger.info(`Successfully found user: ${JSON.stringify(user_response)}`);
  return user_response;
}
