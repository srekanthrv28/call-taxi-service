/* @flow */
'use strict';

import * as logger from 'winston';

import {CreateUserRequestModel} from '../request/create_user_request_model';
import {UserResponse} from '../response/user_response';
import {create_datastore_user} from '../datastore/user_datastore';
import {UserEntity} from '../entity/user_entity';
import * as request_validator from '../request/create_user_request_validator';

export function create_user(request: CreateUserRequestModel): Promise {
  const _x = logger.info(`Creating user with request: ${JSON.stringify(request)}`);
  return request_validator.validate_request_async(request)
    .then(create_datastore_user(request))
    .then(user_creation_success);
}

function user_creation_success(user_entity: UserEntity): UserResponse {
  const _x = logger.info(`Successfully created user: ${JSON.stringify(user_entity)}`);
  return user_entity;
}
