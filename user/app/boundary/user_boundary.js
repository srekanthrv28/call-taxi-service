/* @flow */
'use strict';

import {create_user} from '../interactor/create_user_interactor';
import {CreateUserRequestModel} from '../request/create_user_request_model';
import {FindUserRequest} from '../request/find_user_request';
import {find_user} from '../interactor/find_user_interactor';

export function create_new_user(create_user_request: CreateUserRequestModel): Promise {
  return create_user(create_user_request);
}

export function find_user_by_email(find_user_request: FindUserRequest): Promise {
  return find_user(find_user_request);
}
