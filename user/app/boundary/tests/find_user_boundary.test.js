/* @flow */
'use strict';

import {it} from 'mocha';
import * as user_boundary from '../user_boundary';
import {CreateUserRequest} from '../../request/create_user_request_model';
import {UserResponse} from '../../response/user_response';
import * as faker from 'faker';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);

const get_create_user_request = (): CreateUserRequest => {
  return {
    email: faker.internet.email(),
    phone_number: faker.phone.phoneNumber()
  }
};

const _get_user_by_id_test = it("Should create user and get user email", (): Promise<> => {
  const test_create_request: CreateUserRequest = get_create_user_request();
  const create_user_promise: Promise<> = user_boundary.create_new_user(test_create_request);
  const get_user_promise = create_user_promise.then((user_response: UserResponse): Promise<> => {
    return user_boundary.find_user_by_email(user_response);
  });
  return Promise.all([
    get_user_promise.should.eventually.be.fulfilled,
    get_user_promise.should.eventually.have.property('email').to.equal(test_create_request.email),
  ]);
});
