/* @flow */
'use strict';

import * as user_boundary from '../user_boundary';
import {CreateUserRequest} from '../../request/create_user_request_model';
import * as faker from 'faker';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
import {it} from 'mocha';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);


const get_create_user_request = (): CreateUserRequest => {
    return {
    email: faker.internet.email(),
    phone_number: faker.phone.phoneNumber(),
  }
};

const _create_new_user = it("Should create user with valid create user request", (): Promise<> => {
  const test_create_request: CreateUserRequest = get_create_user_request();
  const create_user_promise: Promise<> = user_boundary.create_new_user(test_create_request);
  return Promise.all([
    create_user_promise.should.eventually.be.fulfilled,
    create_user_promise.should.eventually.have.property('email'),
    create_user_promise.should.eventually.have.property('phone_number')
  ]);
});
