/* @flow */
'use strict';

import logger from 'winston';
import {CreateCabRequest} from "../request/create_cab_request";
import {CabResponse} from "../response/cab_response";
import {CabEntity} from '../entity/cab_entity';
import * as cab_datastore from "../datastore/cab_datastore";
import R from 'ramda';

export function create_or_update_cab(request: CreateCabRequest): Promise<CabResponse | Error> {
  const _x = logger.info(`got the request to create the user cab: ${R.toString(request)}`);
  return cab_datastore.create_datastore_cab(request)
    .then(cab_creation_success);
}

function cab_creation_success(cab_entity: CabEntity): CabResponse {
  const _x = logger.info(`Successfully created cab:  ${R.toString(cab_entity)}`);
  return cab_entity;
}
