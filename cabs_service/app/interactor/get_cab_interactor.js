/* @flow */
'use strict';

import logger from 'winston';
import {CabEntity} from '../entity/cab_entity';
import {GetCabRequest} from '../request/get_cab_request';
import {CabResponse} from '../response/cab_response';
import * as cab_datastore from "../datastore/cab_datastore";
import * as request_validator from '../request/cab_request_validator';
import * as cab_entity_wrapper from './cab_entity_wrapper';

import R from "ramda"

export function get_cab(request: GetCabRequest): Promise<> {
  const _x = logger.info(`got the request to Get the user cab with request: ${R.toString(request)}`);
  return request_validator.validate_get_cab_request_async(request)
    .then(cab_entity_wrapper.from_cab_request_to_entity)
    .then(cab_datastore.get_datastore_cab)
    .then(cab_fetch_success);
}

function cab_fetch_success(cab_entity: CabEntity): CabResponse {
  const _x = logger.info(`successfully getting the user cab details: ${R.toString(cab_entity)}`);
  return cab_entity_wrapper.from_cab_entity_to_response(cab_entity);
}
