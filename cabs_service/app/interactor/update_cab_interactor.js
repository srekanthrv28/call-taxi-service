/* @flow */
'use strict';

import logger from 'winston';
import R from 'ramda';

import {type UpdateCabRequest} from '../request/update_cab_request';
import {type CabEntity} from '../entity/cab_entity';
import {type CabResponse} from '../response/cab_response';
import * as cab_datastore from "../datastore/cab_datastore";
import * as cab_entity_wrapper from './cab_entity_wrapper';

export function update_cab(request: UpdateCabRequest): Promise<> {
  const _x = logger.info(`got the request to update the user cab list with request: ${R.toString(request)}`);
  return cab_datastore.update_cab(request)
        .then(cab_fetch_success);
}

function cab_fetch_success(cab_entity: CabEntity): CabResponse {
  const _y = logger.info(`successfully upadte the cab: ${R.toString(cab_entity)}`);
  return cab_entity_wrapper.from_cab_entity_to_response(cab_entity);
}
