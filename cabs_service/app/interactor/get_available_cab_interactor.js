/* @flow */
'use strict';

import logger from 'winston';
import {CabEntity} from '../entity/cab_entity';
import * as cab_datastore from "../datastore/cab_datastore";

import R from "ramda"

export function get_availble_cabs(): Promise<> {
  const _x = logger.info(`got the request to Get the availble cab with request`);
  return cab_datastore.get_availble_datastore_cabs()
    .then(cab_fetch_success);
}


export function get_pink_availble_cabs(): Promise<> {
  const _x = logger.info(`got the request to Get the pink availble cab with request`);
  return cab_datastore.get_pink_availble_cabs_from_datastore()
    .then(cab_fetch_success);
}

export function get_normal_availble_cabs(): Promise<> {
  const _x = logger.info(`got the request to Get the normal availble cab with request`);
  return cab_datastore.get_normal_availble_cabs_from_datastore()
    .then(cab_fetch_success);
}

function cab_fetch_success(cab_entities: Array<CabEntity>): Array<CabEntity> {
  const _x = logger.info(`successfully getting the availble cab details: ${R.toString(cab_entities)}`);
  return cab_entities;
}
