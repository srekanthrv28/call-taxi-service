/* @flow */
'use strict';

import {CreateCabRequest} from '../request/create_cab_request';
import {InitialCabResponse,type CabResponse} from '../response/cab_response';
import {IntialCabEntity,type CabEntity} from '../entity/cab_entity';
import R from 'ramda';

export function from_cab_entity_to_response(cab_entity: CabEntity): CabResponse {
  if (cab_entity) {
    return R.pick(R.keys(InitialCabResponse), cab_entity);
  }
  return InitialCabResponse;
}

export function from_cab_request_to_entity(request: CreateCabRequest): CabEntity {
  return R.pick(R.keys(IntialCabEntity), request);
}
