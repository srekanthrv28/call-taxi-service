/* @flow */
'use strict';

import * as create_cab_interactor from '../interactor/create_cab_interactor';
import * as get_cab_interactor from '../interactor/get_cab_interactor';
import * as update_cab_interactor from '../interactor/update_cab_interactor';
import * as get_available_cab_interactor from '../interactor/get_available_cab_interactor'

import {CreateCabRequest} from '../request/create_cab_request';
import {GetCabRequest} from '../request/get_cab_request';

export function create_cab(request_model: CreateCabRequest): Promise<> {
  return create_cab_interactor.create_or_update_cab(request_model);
}

export function get_cab(request_model: GetCabRequest): Promise<> {
  return get_cab_interactor.get_cab(request_model);
}

export function update_cab(request_model: GetCabRequest): Promise<> {
  return update_cab_interactor.update_cab(request_model);
}

export function book_cab(request_model: CreateCabRequest): Promise<> {
  return update_cab_interactor.update_cab(request_model);
}

export function get_availble_cabs(): Promise<> {
  return get_available_cab_interactor.get_availble_cabs();
}

export function get_pink_availble_cabs(): Promise<> {
  return get_available_cab_interactor.get_pink_availble_cabs();
}

export function get_normal_availble_cabs(): Promise<> {
  return get_available_cab_interactor.get_normal_availble_cabs();
}
