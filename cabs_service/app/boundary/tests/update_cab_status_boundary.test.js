/* @flow */
'use strict';

import {it} from 'mocha';
import * as cab_boundary from '../cab_boundary';
import {CreateCabRequest} from '../../request/create_cab_request';
import {GetCabRequest} from '../../request/get_cab_request';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);

const get_create_cab_request = (): CreateCabRequest => {
  return {
    cab_type: 'Normal',
    lat: '12.70',
    lang: '15.60',
    is_booked: false
  }
};

const get_cab_id_for_get_request = (cab_request: CreateCabRequest): GetCabRequest => {
  return {
    cab_id: cab_request.cab_id,
    cab_type: 'pink',
    lat: '12.70',
    lang: '15.60',
    is_booked: true
  }
};

 const _update_cab = it('Should update user cab with valid create user cab request', (): Promise<> => {
    const test_create_request: CreateCabRequest = get_create_cab_request();
    const create_cab_promise: Promise<> = cab_boundary.create_cab(test_create_request);
    const get_user_promise = create_cab_promise.then((cab_response: CreateCabRequest): Promise<> => {
      const test_get_request: GetCabRequest = get_cab_id_for_get_request(cab_response);
      const get_cab_promise = cab_boundary.update_cab(test_get_request);
      const get_availble_cabs_promise: Promise<> = cab_boundary.get_availble_cabs();
      return Promise.all([
        get_cab_promise.should.eventually.be.fulfilled,
        get_availble_cabs_promise.should.eventually.be.fulfilled,
        get_cab_promise.should.eventually.have.property('cab_id')
      ]);
    });
   return get_user_promise;
});
