/* @flow */
'use strict';

import {it} from 'mocha';
import * as cab_boundary from '../cab_boundary';
import {CreateCabRequest} from '../../request/create_cab_request';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);

const get_create_cab_request = (): CreateCabRequest => {
  return {
    cab_type: 'Normal',
    lat: '12.70',
    lang: '15.60',
    is_booked: false
  }
};

 const _create_new_cab = it('Should create user cab with valid create user cab request', (): Promise<> => {
    const test_create_request: CreateCabRequest = get_create_cab_request();
    const create_cab_promise: Promise<> = cab_boundary.create_cab(test_create_request);
    return Promise.all([
      create_cab_promise.should.eventually.be.fulfilled,
      create_cab_promise.should.eventually.have.property('cab_type')
    ]);
});
