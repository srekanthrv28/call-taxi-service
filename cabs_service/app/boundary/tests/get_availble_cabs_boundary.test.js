/* @flow */
'use strict';

import {it} from 'mocha';
import * as cab_boundary from '../cab_boundary';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);

const _get_cab = it('Should create user cab with valid create cab request', (): Promise<> => {
  const get_cab_promise: Promise<> = cab_boundary.get_availble_cabs();
  return Promise.all([
     get_cab_promise.should.eventually.be.fulfilled
  ]);
});
