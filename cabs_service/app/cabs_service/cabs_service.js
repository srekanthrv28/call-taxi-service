/* @flow */
'use strict';

import logger from 'winston';
import initial_cabs_data from './initial_cabs_data';
import * as cab_boundary from '../boundary/cab_boundary';
import R from 'ramda';


const _data = logger.info('loading the intial data to memory', initial_cabs_data);

const cabs_service = R.map(insert_cab_data_into_memory, initial_cabs_data);

function insert_cab_data_into_memory(cab_data: *): * {
  const _log = logger.info("insert_cab_data_into_memory", cab_data);
  return cab_boundary.create_cab(cab_data);
}

const _completed_data = logger.info('loding done...', cabs_service);

export default cabs_service;
