/* @flow */
'use strict';

const initial_cabs_data = [
  {
    cab_id: 10000,
    cab_type: "normal",
    lat: '12.9260',
    lang: '77.6762',
    is_booked: false
  },
  {
    cab_id: 10001,
    cab_type: "normal",
    lat: '13.0358',
    lang: '77.5970',
    is_booked: false
  },
  {
    cab_id: 10002,
    cab_type: "normal",
    lat: '12.9279',
    lang: '77.6271',
    is_booked: false
  },
  {
    cab_id: 10003,
    cab_type: "pink",
    lat: '12.9279',
    lang: '77.6271',
    is_booked: false
  },
  {
    cab_id: 10004,
    cab_type: "pink",
    lat: '12.9260',
    lang: '77.6762',
    is_booked: false
  }
];

export default initial_cabs_data;
