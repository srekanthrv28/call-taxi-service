/* @flow */
'use strict';

export interface CabEntity {
  +cab_id: string,
  +cab_type: string,
  +lat: string,
  +lan: string,
  +is_booked: boolean
}

export const IntialCabEntity: CabEntity = {
  cab_id: '',
  cab_type: '',
  lat: '',
  lan: '',
  is_booked: false
};
