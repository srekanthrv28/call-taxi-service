/* @flow */
'use strict';

export interface UpdateCabRequest {
  +cab_id: string,
  +cab_type: string,
  +lat: string,
  +lan: string
}

export const InitialUpdateCabRequest: UpdateCabRequest = {
  cab_id: '',
  cab_type: '',
  lat: '',
  lan: ''
};


export const UpdateCabRequestConstraints  = {
  cab_id: {
    presence: true,
    length: {
      minimum: 5,
      maximum: 512
    }
  }
}
