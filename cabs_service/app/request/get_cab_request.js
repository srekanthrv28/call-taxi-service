/* @flow */
'use strict';

export interface GetCabRequest {
  +cab_id: string
}

export const InitialGetCabRequest: GetCabRequest = {
  cab_id: ''
};

export const GetCabRequestConstraints  = {
  cab_id: {
    presence: true,
    length: {
      minimum: 5,
      maximum: 512
    }
  }
}
