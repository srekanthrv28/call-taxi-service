/* @flow */
'use strict';

export interface CreateCabRequest {
  +cab_id: string,
  +cab_type: string,
  +lat: string,
  +lan: string
}

export const InitialCreateCabRequest: CreateCabRequest = {
  cab_id: '',
  cab_type: '',
  lat: '',
  lan: ''
};


export const CreateCabRequestConstraints  = {
  cab_id: {
    presence: true
  }
}
