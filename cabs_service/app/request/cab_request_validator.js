/* @flow */
'use strict';

import {CreateCabRequest,CreateCabRequestConstraints} from '../request/create_cab_request';
import {GetCabRequest,GetCabRequestConstraints} from '../request/get_cab_request';
import {UpdateCabRequest,UpdateCabRequestConstraints} from '../request/update_cab_request';
import * as validate from 'validate.js';
import R from 'ramda'


export function validate_create_cab_request_async(create_cab_request: CreateCabRequest): Promise<> {
  const validation_result: mixed = validate.validate(create_cab_request, CreateCabRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(create_cab_request);
  }
  return send_error(validation_result);
}

export function validate_get_cab_request_async(get_cab_request: GetCabRequest): Promise<> {
  const validation_result: mixed = validate.validate(get_cab_request, GetCabRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(get_cab_request);
  }
  return send_error(validation_result);
}

export function validate_update_cab_request_async(update_cab_request: UpdateCabRequest): Promise<> {
  const validation_result: mixed = validate.validate(update_cab_request, UpdateCabRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(update_cab_request);
  }
  return send_error();
}

function send_error(): Promise<>  {
  return Promise.reject({reason: "Invalid Request", type: "INVALID_REQUEST"});
}
