/* @flow */
'use strict';

export interface GetCabResponse {
  +cab_id: string,
  +cab: Array<string>
}
