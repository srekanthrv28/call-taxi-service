/* @flow */
'use strict';

export interface CabResponse {
  +cab_id: string,
  +cab: Array<string>
}

export const InitialCabResponse: CabResponse = {
  cab_id: '',
  cab: []
};
