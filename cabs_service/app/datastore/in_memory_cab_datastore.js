/* @flow */
'use strict';

import {CabEntity} from '../entity/cab_entity';
import * as datastore from 'memory-cache';
import R from 'ramda';
import logger from 'winston';
import uuid_v4 from 'uuid/v4';

export function create_datastore_cab(cab_entity: CabEntity): Promise<> {
  return R.ifElse(R.curry(does_cab_already_exist), send_empty_response, create_new_datastore_cab)(cab_entity);
}

export function get_datastore_cab(cab_entity: CabEntity): Promise<> {
  return R.ifElse(R.curry(does_cab_already_exist), get_cab_from_db, send_empty_response)(cab_entity);
}

function get_cab_from_db(cab_entity: CabEntity): CabEntity {
  return datastore.get(cab_entity.cab_id);
}

export function get_availble_cabs_from_datastore(): Array<CabEntity> {
  if(R.isNil(datastore.get("available_cars")) || R.isEmpty(datastore.get("available_cars"))) {
    return Promise.reject({message: "No avaialble Cars"});
  }
  return Promise.resolve(datastore.get("available_cars"));
}

export function get_normal_availble_cabs_from_datastore(): Array<CabEntity> {
  if(R.isNil(datastore.get("available_cars")) || R.isEmpty(datastore.get("available_cars"))) {
    return Promise.reject({message: "No avaialble Cars"});
  }
  return R.filter(is_pink_cab, datastore.get("available_cars"));
}


export function get_pink_availble_cabs_from_datastore(): Array<CabEntity> {
  if(R.isNil(datastore.get("available_cars")) || R.isEmpty(datastore.get("available_cars"))) {
    return Promise.reject({message: "No avaialble Cars"});
  }
  return R.filter(is_normal_cab, datastore.get("available_cars"));
}

function is_normal_cab(cab: CabEntity): CabEntity {
  return R.equals(cab.cab_type == "normal");
}

function is_pink_cab(cab: CabEntity): CabEntity {
  return R.equals(cab.cab_type == "pink");
}

function error_user_doesnt_exist(cab_entity: CabEntity): Promise<Error> {
  const _x = logger.error("Cab with cab_id: ", R.toString(cab_entity.cab_id), " doesn't exist..." );
  return Promise.reject({reason: `User with cab_id: ${cab_entity.cab_id} doesn't exist...`, type: "INVALID_REQUEST"});
}

function send_empty_response(cab_entity: CabEntity): Promise<> {
  const _x = logger.warn("Cab with cab_id: ", R.toString(cab_entity.cab_id), " doesn't exist..." );
  return Promise.resolve({});
}

function does_cab_already_exist(cab_entity: CabEntity): boolean {
  return R.ifElse(R.curry(datastore.get), R.T, R.F)(cab_entity.cab_id);
}

function create_new_datastore_cab(cab_entity: CabEntity): Promise<> {
  const _x = logger.error("coming here..." );
  const new_cab_entity: CabEntity = R.merge(cab_entity, {cab_id: uuid_v4()});
  const new_cab = datastore.put(new_cab_entity.cab_id, new_cab_entity);
  const _update =  update_new_cab_availble_cars(new_cab)
  return Promise.resolve(new_cab);
}

function update_new_cab_availble_cars(new_cab: CabEntity): Promise<> {
  const available_cars = datastore.get("available_cars");
  if(available_cars) {
    const updated_available_cars = R.append(new_cab, available_cars);
    const update_done1 = datastore.put("available_cars", updated_available_cars);
    return Promise.resolve(update_done1);
  }
    const updated_available_cars = R.append(new_cab, []);
    const update_don2 = datastore.put("available_cars", updated_available_cars);
    return Promise.resolve(update_don2);
}

export function update_datastore_cab(cab_entity: CabEntity): Promise<> {
  return R.ifElse(R.curry(does_cab_already_exist), update_cab, error_user_doesnt_exist)(cab_entity);
}

function update_cab(cab_entity: CabEntity): Promise<> {
  const existing_cab = datastore.get(cab_entity.cab_id);
  const updated_cab = R.merge(existing_cab, cab_entity);
  if(updated_cab.is_booked) {
   const _x = check_and_remove_from_the_available_cars(updated_cab);
  }
  return Promise.resolve(datastore.put(cab_entity.cab_id, updated_cab));
}


function check_and_remove_from_the_available_cars(updated_cab: CabEntity): Promise<> {
  const available_cars = datastore.get("available_cars");
  const updated_available_cars = R.filter(R.curry(is_cab_exists)(updated_cab.cab_id), available_cars);
  const upadate = datastore.put(available_cars, updated_available_cars)
  return upadate;
}

function is_cab_exists(cab_id1: string, cab: string): boolean {
  return R.equals(cab_id1, cab.cab_id);
}
