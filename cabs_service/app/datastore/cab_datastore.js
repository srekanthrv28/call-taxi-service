/* @flow */
'use strict';

import {CabEntity} from '../entity/cab_entity';
import * as in_memory_datastore from './in_memory_cab_datastore';



export function create_datastore_cab(cab_entity: CabEntity): Promise<> {
  return in_memory_datastore.create_datastore_cab(cab_entity);
}

export function get_datastore_cab(cab_entity: CabEntity): Promise<> {
  return in_memory_datastore.get_datastore_cab(cab_entity);
}

export function update_cab(cab_entity: CabEntity): Promise<> {
  return in_memory_datastore.update_datastore_cab(cab_entity);
}

export function get_availble_datastore_cabs(): Promise<> {
   return in_memory_datastore.get_availble_cabs_from_datastore();
}

export function get_normal_availble_cabs_from_datastore(): Promise<> {
   return in_memory_datastore.get_availble_cabs_from_datastore();
}

export function get_pink_availble_cabs_from_datastore(): Promise<> {
   return in_memory_datastore.get_availble_cabs_from_datastore();
}
