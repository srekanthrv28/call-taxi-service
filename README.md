Call taxi service

Download and install node version 6.10.x and above

Install YARN the node package manager

Clone the project

Run "yarn dev" inside the project

Server will be running on http://127.0.0.1:3000/

Run "yarn test" on each module to validate unit test cases.
