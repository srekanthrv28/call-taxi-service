/* @flow */
'use strict';

import {cab_boundary} from 'cabs_service';
import {location_handler_boundary, LocationDistanceHandlerRequest} from 'location_handler';
import {
  type $Request as ExpressRequest,
  type $Response as ExpressResponse
} from 'express';
import * as web_response_wrapper from '../core/web_response_wrapper';
import R from 'ramda';

export function create_cab(request: ExpressRequest, response: ExpressResponse): ExpressResponse {
  return cab_boundary.create_cab(request.body)
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}

export function get_availble_cabs(request: ExpressRequest, response: ExpressResponse): ExpressResponse {
  return cab_boundary.get_availble_cabs()
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}


export function book_cab(request: ExpressRequest, response: ExpressResponse): ExpressResponse {
  return cab_boundary.get_availble_cabs()
    .then(R.curry(get_distance_request)(request.body))
    .then(location_handler_boundary.find_distance)
    .then(get_book_request)
    .then(cab_boundary.book_cab)
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}

export function book_normal_cab(request: ExpressRequest, response: ExpressResponse): ExpressResponse {
  return cab_boundary.get_normal_availble_cabs()
    .then(R.curry(get_distance_request)(request.body))
    .then(location_handler_boundary.find_distance)
    .then(get_book_request)
    .then(cab_boundary.book_cab)
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}

export function book_pink_cab(request: ExpressRequest, response: ExpressResponse): ExpressResponse {
  return cab_boundary.get_pink_availble_cabs()
    .then(R.curry(get_distance_request)(request.body))
    .then(location_handler_boundary.find_distance)
    .then(get_book_request)
    .then(cab_boundary.book_cab)
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}

  function get_distance_request(request: ExpressRequest, response: *): LocationDistanceHandlerRequest{
    const origin = {lat: request.body.latitude, lng: request.body.longitude}
    return {origin: origin, destination: response}
 }

 function get_book_request(response: *): *{
   return {cab_id: response.cab_details.cab_id, is_booked: true}
}
