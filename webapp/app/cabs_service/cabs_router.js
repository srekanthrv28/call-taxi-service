/* @flow */
'use strict';
import * as express from 'express';
import * as cab_service_controller from './cab_service_controller';

const router: express.Router = express.Router();


/* /cab/create:
 *   post:
 *     tags:
 *     description: create cab
 *     parameters:
 *       - json
 *         in: body
 *         required: true
 *         schema:
 *           required: true
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: create cab
 */

const _y = router.post('/create', (req: express$Request, res: express$Response): number => {
  const _updated_response = cab_service_controller.create_cab(req, res);
  return 0;
});


/* /cab/get_availble_cabs:
 *   post:
 *     tags:
 *     description: create cab
 *     parameters:
 *       - json
 *         in: body
 *         required: true
 *         schema:
 *           required: true
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: create cab
 */

const _x1 = router.get('/get_availble_cabs', (req: express$Request, res: express$Response): number => {
  const _updated_response = cab_service_controller.get_availble_cabs(req, res);
  return 0;
});



/* /cab/get_availble_cabs:
 *   post:
 *     tags:
 *     description: create cab
 *     parameters:
 *       - json
 *         in: body
 *         required: true
 *         schema:
 *           required: true
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: create cab
 */

const _x2 = router.post('/book_cab', (req: express$Request, res: express$Response): number => {
  const _updated_response = cab_service_controller.book_cab(req, res);
  return 0;
});



/* /cab/get_availble_cabs:
 *   post:
 *     tags:
 *     description: create cab
 *     parameters:
 *       - json
 *         in: body
 *         required: true
 *         schema:
 *           required: true
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: create cab
 */

const _x3 = router.post('/book_cab/normal', (req: express$Request, res: express$Response): number => {
  const _updated_response = cab_service_controller.book_cab(req, res);
  return 0;
});


/* /cab/get_availble_cabs:
 *   post:
 *     tags:
 *     description: create cab
 *     parameters:
 *       - json
 *         in: body
 *         required: true
 *         schema:
 *           required: true
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: create cab
 */

const _x4 = router.post('/book_cab/pink', (req: express$Request, res: express$Response): number => {
  const _updated_response = cab_service_controller.book_cab(req, res);
  return 0;
});

export {
  router
};
