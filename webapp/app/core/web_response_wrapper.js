/* @flow */
'use strict';

import {WebResponse} from './web_response';
import {
  type $Request as ExpressRequest,
  type $Response as ExpressResponse
} from 'express';

export function send_success_response<T>(request: ExpressRequest, response: ExpressResponse, user_profile_response: T): ExpressResponse {
  return response.send(build_successful_response(user_profile_response));
}

export function send_failed_response(response: ExpressResponse, error: Error | Array | string): ExpressResponse {
  return response.send(build_failed_response(error));
}

function build_successful_response<T>(response: T): WebResponse<T> {
  const web_response: WebResponse<T> = {
    success: true,
    response: response
  }
  return web_response;
}

function build_failed_response<T>(input_errors: string | Error | Array): WebResponse<T> {
  const errors = build_error_messages(input_errors);
  const web_response: WebResponse<T> = {
    success: false,
    errors: errors
  }
  return web_response;
}

function build_error_messages(errors: string | Error | Array): Array<string> {
  if (typeof errors == Error) {
    return [errors.message];
  }
  else if (typeof errors == 'string') {
    return [errors];
  }
  return [errors];
}
