/* @flow */
'use strict';


export interface WebResponse<ResponseType> {
  +success: boolean,
  +errors: Array<string>,
  +response: ResponseType
}
