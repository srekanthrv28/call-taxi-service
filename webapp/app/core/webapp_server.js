/* @flow */
'use strict';

import * as webapp_routes from '../routes/webapp_router';
import express from 'express';
import * as logger from 'winston';
import body_parser from 'body-parser';

const __public_dir = __dirname + '/../../public';

const port = process.env.PORT || 3000;

const webapp_server = express();

const _a = webapp_server.use(body_parser.json());

const _x = webapp_server.use(express.static(__public_dir));

const _y = webapp_routes.add_all_web_app_routes(webapp_server);

const _z = webapp_server.listen(port, (): number => {
  const _x = logger.info('Test App Server Listening on port', port);
  return 0;
});

export default webapp_server;
