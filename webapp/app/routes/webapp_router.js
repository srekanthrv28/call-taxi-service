/* @flow */
'use strict';

import {router as user_router} from '../user/user_router';
import {router as cabs_service_router} from '../cabs_service/cabs_router';
import type {
  $Application,
  Router,
  $Request as ExpressRequest,
  $Response as ExpressResponse
} from 'express';

export function add_all_web_app_routes(app: $Application): number {
  const _x = add_generic_routes(app);
  const _z = app.use('/user', user_router);
  const _y = app.use('/cab', cabs_service_router);
  return 0;
}

function add_generic_routes(app: Router): number {
  const _x = app.get('/test', (request: ExpressRequest, response: ExpressResponse): ExpressResponse => {
    const _udpated_response = response.send({
      name: 'Sreekanth',
      properties: [{
        key: 'Sreekanth',
        value: 'R V'
      }]
    });
    return response;
  });
  return 0;
}
