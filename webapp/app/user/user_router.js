/* @flow */
'use strict';
import * as express from 'express';
import * as user_profile_controller from './user_profile_controller';

const router: express.Router = express.Router();

// middleware that is specific to this router
const _x = router.get('/', (req: express$Request, res: express$Response): number => {
  const _updated_response = res.send('User home Page');
  return 0;
});

const _y = router.post('/', (req: express$Request, res: express$Response): number => {
  const _updated_response = user_profile_controller.create_user_profile(req, res);
  return 0;
});

export {
  router
};
