/* @flow */
'use strict';

import {
  user_boundary
} from 'user';

import {
  type $Request as ExpressRequest,
  type $Response as ExpressResponse
} from 'express';
import * as web_response_wrapper from '../core/web_response_wrapper';
import R from 'ramda';

export function create_user_profile(request: ExpressRequest, response: ExpressResponse): ExpressResponse {
  return user_boundary.create_new_user(request)
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}
