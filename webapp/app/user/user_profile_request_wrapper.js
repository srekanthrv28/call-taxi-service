/* @flow */
'use strict';
import {type $Request as ExpressRequest} from 'express';
import {type CreateUserRequestModel} from 'user';

export function from_web_request_to_create_user_request(request: ExpressRequest): CreateUserRequestModel {
  const create_user_request: CreateUserRequestModel = {
    email: request.body.email,
    phone_number: request.body.phone_number
  };
  return create_user_request;
}
